#pragma once
#include <exception>
#include <string>

namespace scvault
{
    class exception_type: public std::exception
    {
    public:
        explicit exception_type(std::string_view message):
            _message(message)
        {
            // Nothing to do yet
        }

        virtual const char* what() const noexcept override
        {
            return _message.c_str();
        }

    private:
        std::string _message;
    };
}
