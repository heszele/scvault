#pragma once
#include "types.hh"

#include <filesystem>
#include <fstream>

namespace scvault
{
    uint32_t from_byte_stream(const byte_type* data);
    void to_byte_stream(uint32_t number, byte_type data[4]);

    //
    // binary_istream_type
    //

    class binary_istream_type
    {
    public:
        binary_istream_type() = default;
        binary_istream_type(const binary_istream_type&) = delete;
        binary_istream_type(binary_istream_type&&) = delete;

        virtual ~binary_istream_type() = default;

        binary_istream_type& operator=(const binary_istream_type&) = delete;
        binary_istream_type& operator=(binary_istream_type&&) = delete;

        operator bool() const
        {
            return to_bool();
        }

        virtual binary_istream_type& read(byte_type* data, size_t size) = 0;

        [[nodiscard]] virtual bool to_bool() const = 0;
        [[nodiscard]] virtual bool eof() const = 0;
        [[nodiscard]] virtual std::streamsize gcount() const = 0;
        [[nodiscard]] virtual bool fail() const = 0;
    };

    //
    // binary_ifstream_type
    //

    class binary_ifstream_type: public binary_istream_type
    {
    public:
        binary_ifstream_type() = default;
        explicit binary_ifstream_type(const std::filesystem::path& path,
                                      std::ios_base::openmode open_mode = std::ios_base::binary);
        explicit binary_ifstream_type(const std::string& path,
                                      std::ios_base::openmode open_mode = std::ios_base::binary);

        virtual binary_ifstream_type& read(byte_type* data, size_t size) override;

        [[nodiscard]] virtual bool to_bool() const override;
        [[nodiscard]] virtual bool eof() const override;
        [[nodiscard]] virtual std::streamsize gcount() const override;
        [[nodiscard]] virtual bool fail() const override;
        [[nodiscard]] size_t tellg();

        void seekg(int32_t off, std::ios_base::seekdir dir);
        void close();

    private:
        std::ifstream _stream;
    };

    //
    // binary_istringstream_type
    //

    class binary_istringstream_type: public binary_istream_type
    {
    public:
        binary_istringstream_type(const byte_type* data, size_t size);

        virtual binary_istringstream_type& read(byte_type* data, size_t size) override;

        [[nodiscard]] virtual bool to_bool() const override;
        [[nodiscard]] virtual bool eof() const override;
        [[nodiscard]] virtual std::streamsize gcount() const override;
        [[nodiscard]] virtual bool fail() const override;

    private:
        std::vector<byte_type> _data;
        std::vector<byte_type>::const_iterator _position;
        std::streamsize _gcount = 0;
    };

    //
    // binary_ostream_type
    //

    class binary_ostream_type
    {
    public:
        binary_ostream_type() = default;
        binary_ostream_type(const binary_ostream_type&) = delete;
        binary_ostream_type(binary_ostream_type&&) = delete;

        virtual ~binary_ostream_type() = default;

        binary_ostream_type& operator=(const binary_ostream_type&) = delete;
        binary_ostream_type& operator=(binary_ostream_type&&) = delete;

        operator bool() const
        {
            return to_bool();
        }

        virtual binary_ostream_type& write(const byte_type* data, size_t size) = 0;

        [[nodiscard]] virtual bool to_bool() const = 0;
        [[nodiscard]] virtual bool eof() const = 0;
        [[nodiscard]] virtual bool fail() const = 0;
    };

    //
    // binary_ofstream_type
    //

    class binary_ofstream_type: public binary_ostream_type
    {
    public:
        explicit binary_ofstream_type(const std::filesystem::path& path,
                                      std::ios_base::openmode open_mode = std::ios_base::binary);
        explicit binary_ofstream_type(const std::string& path,
                                      std::ios_base::openmode open_mode = std::ios_base::binary);

        virtual binary_ofstream_type& write(const byte_type* data, size_t size) override;

        [[nodiscard]] virtual bool to_bool() const override;
        [[nodiscard]] virtual bool eof() const override;
        [[nodiscard]] virtual bool fail() const override;

        std::streampos tellp()
        {
            return _stream.tellp();
        }

        void seekp(std::streamoff offset, std::ios_base::seekdir seek_direction)
        {
            _stream.seekp(offset, seek_direction);
        }

    private:
        std::ofstream _stream;
    };

    //
    // binary_ostringstream_type
    //

    class binary_ostringstream_type: public binary_ostream_type
    {
    public:
        virtual binary_ostringstream_type& write(const byte_type* data, size_t size) override;

        [[nodiscard]] virtual bool to_bool() const override;
        [[nodiscard]] virtual bool eof() const override;
        [[nodiscard]] virtual bool fail() const override;

        [[nodiscard]] const std::vector<byte_type>& data() const;

    private:
        std::vector<byte_type> _data;
    };
}
