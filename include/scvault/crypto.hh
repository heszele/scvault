#pragma once
#include "secure_data.hh"
#include "types.hh"
#include "stream.hh"

#include <sodium.h>

#include <vector>
#include <string>
#include <algorithm>
#include <filesystem>

namespace scvault::crypto
{
    using hash_type = array_type<crypto_generichash_BYTES>;
    using salt_type = array_type<crypto_pwhash_SALTBYTES>;
    using seed_type = array_type<crypto_box_SEEDBYTES>;
    using public_key_type = array_type<crypto_box_PUBLICKEYBYTES>;
    using nonce_type = array_type<crypto_box_NONCEBYTES>;
    using key_type = array_type<crypto_secretstream_xchacha20poly1305_KEYBYTES>;
    using encrypted_key_type = array_type<crypto_box_MACBYTES + crypto_secretstream_xchacha20poly1305_KEYBYTES>;
    using header_type = array_type<crypto_secretstream_xchacha20poly1305_HEADERBYTES>;

    class secret_key_type: public secure_data_type<byte_type>
    {
    public:
        secret_key_type():
            secure_data_type<byte_type>(crypto_box_SECRETKEYBYTES)
        {
            // Nothing to do yet
        }

        ~secret_key_type() = default;

        secret_key_type(const secret_key_type&) = default;
        secret_key_type(secret_key_type&&) = default;

        secret_key_type& operator=(const secret_key_type&) = default;
        secret_key_type& operator=(secret_key_type&&) = default;
    };

    [[nodiscard]] result_type<> encrypt(binary_istream_type& input, binary_ostream_type& output,
                                        const public_key_type& public_key, const secret_key_type& secret_key);
    [[nodiscard]] result_type<> encrypt(binary_istream_type& input, binary_ostream_type& output,
                                        const key_type& file_key);

    [[nodiscard]] result_type<> decrypt(binary_istream_type& input, binary_ostream_type& output,
                                        const public_key_type& public_key, const secret_key_type& secret_key);
    [[nodiscard]] result_type<> decrypt(binary_istream_type& input, binary_ostream_type& output,
                                        const key_type& file_key);

    template<typename buffer_type>
    std::string to_base64(const buffer_type& data)
    {
        const size_t size = sodium_base64_ENCODED_LEN(data.size(), sodium_base64_VARIANT_ORIGINAL);
        std::vector<char> base64(size);

        sodium_bin2base64(base64.data(), size,
                          data.data(), data.size(),
                          sodium_base64_VARIANT_ORIGINAL);

        // replace '/' with '_'
        std::replace_if(base64.begin(), base64.end(), [](char c) { return c == '/'; }, '_');

        return std::string(base64.data());
    }

    std::string obfuscate(const std::string& string);
}
