#pragma once
#include "crypto.hh"
#include "types.hh"

#include <filesystem>
#include <vector>

namespace scvault
{
    class shared_file_type
    {
    public:
        [[nodiscard]] static result_type<> initialize(const std::filesystem::path& path,
                                                      const crypto::public_key_type& owner_public_key,
                                                      const crypto::secret_key_type& owner_secret_key);
        explicit shared_file_type(std::filesystem::path path,
                                  crypto::public_key_type public_key,
                                  crypto::secret_key_type secret_key);

        [[nodiscard]] size_t get_encrypted_content_size() const;

        [[nodiscard]] result_type<> read(byte_type* data, size_t size) const;
        [[nodiscard]] result_type<> write(const byte_type* data, size_t size);

        [[nodiscard]] result_type<> share(const crypto::public_key_type& public_key);
        [[nodiscard]] result_type<> unshare(const crypto::public_key_type& public_key);

    private:
        struct footer_type
        {
            crypto::nonce_type _nonce;
            crypto::public_key_type _public_key;
            crypto::public_key_type _inviter_public_key;
            crypto::encrypted_key_type _encrypted_file_key;

            bool operator==(const footer_type& other) const
            {
                return _nonce == other._nonce &&
                       _public_key == other._public_key &&
                       _encrypted_file_key == other._encrypted_file_key;
            }
        };

        struct encrypt_key_result_type
        {
            crypto::nonce_type _nonce;
            crypto::encrypted_key_type _encrypted_key;
        };

        static result_type<encrypt_key_result_type> encrypt_key(const crypto::key_type& key,
                                                                   const crypto::public_key_type& public_key,
                                                                   const crypto::secret_key_type& secret_key);
        static void write_footers(binary_ostream_type& output, const std::vector<footer_type>& footers);
        static result_type<footer_type> get_footer(const crypto::public_key_type& public_key,
                                                   const std::vector<footer_type>& footers,
                                                   const std::filesystem::path& path);

        void write_footers(binary_ostream_type& output) const;
        [[nodiscard]] result_type<crypto::key_type> get_file_key() const;
        [[nodiscard]] result_type<footer_type> get_footer() const;
        [[nodiscard]] bool is_owner() const;

        std::filesystem::path _path;
        std::vector<footer_type> _footers;
        size_t _encrypted_content_size = 0;
        crypto::public_key_type _public_key;
        crypto::secret_key_type _secret_key;
    };
}
