#pragma once
#include "secure_data.hh"
#include "crypto.hh"
#include "types.hh"

#include <rapidjson/document.h>

#include <filesystem>

namespace scvault
{
    class credentials_type
    {
    public:
        using password_type = crypto::secure_data_type<char>;

        credentials_type(std::string username, password_type password);

        [[nodiscard]] const std::string& get_username() const;
        [[nodiscard]] password_type::reference_type get_password() const;

    private:
        std::string _username;
        password_type _password;
    };

    class vault_type
    {
    public:
        [[nodiscard]] static bool is_vault(const std::filesystem::path& path);
        [[nodiscard]] static result_type<> initialize(const std::filesystem::path& path, const credentials_type& admin);

        explicit vault_type(std::filesystem::path path);
        [[nodiscard]] result_type<> login(const credentials_type& credentials);

    private:
        [[nodiscard]] static result_type<> check(const std::filesystem::path& path, bool initialize);
        [[nodiscard]] static std::string to_string(const rapidjson::Document& document);
        [[nodiscard]] static result_type<> generate_key_pair(const credentials_type& credentials,
                                                             const crypto::salt_type& salt,
                                                             crypto::public_key_type& public_key,
                                                             crypto::secret_key_type& secret_key);

        const std::filesystem::path _path;
    };
}
