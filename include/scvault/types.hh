#pragma once
#include <array>
#include <vector>
#include <string>

#include <tl/expected.hpp>

namespace scvault
{
    using byte_type = uint8_t;
    template<size_t SIZE>
    using array_type = std::array<byte_type, SIZE>;
    using vector_type = std::vector<byte_type>;

    template<typename value_type = void>
    using result_type = tl::expected<value_type, std::string>;

    template<size_t SIZE>
    constexpr size_t size_of(const array_type<SIZE>&)
    {
        return SIZE;
    }
}
