#pragma once
#include "exception.hh"

#include <sodium.h>
#include <fmt/format.h>
#include <spdlog/spdlog.h>

namespace scvault::crypto
{
    template<typename buffer_type>
    class secure_data_type
    {
        class control_block_type;
    public:
        class reference_type
        {
        public:
            explicit reference_type(const secure_data_type& data):
                _control_block(data._control_block)
            {
                increment();
            }

            reference_type(const reference_type& other):
                _control_block(other._control_block)
            {
                increment();
            }

            reference_type(reference_type&& other) noexcept:
                _control_block(other._control_block)
            {
                other._control_block = nullptr;
            }

            ~reference_type()
            {
                decrement();
            }

            reference_type& operator=(const reference_type& other)
            {
                if(this == &other)
                {
                    return *this;
                }

                decrement();
                _control_block = other._control_block;
                increment();

                return *this;
            }

            reference_type& operator=(reference_type&& other) noexcept
            {
                if(this == &other)
                {
                    return *this;
                }

                decrement();
                _control_block = other._control_block;
                other._control_block = nullptr;

                return *this;
            }

            secure_data_type* operator->()
            {
                if(nullptr == _control_block || _control_block->_destroyed)
                {
                    return nullptr;
                }

                return _control_block->_data;
            }

            [[nodiscard]] bool is_valid() const
            {
                return nullptr != _control_block && !_control_block->_destroyed;
            }

            void release()
            {
                decrement();
                _control_block = nullptr;
            }

        private:
            void increment()
            {
                if(nullptr != _control_block)
                {
                    _control_block->increment();
                }
            }

            void decrement()
            {
                if(nullptr != _control_block)
                {
                    _control_block->decrement();
                }
            }

            control_block_type* _control_block = nullptr;
        };

        explicit secure_data_type(size_t size):
            _data(static_cast<buffer_type*>(sodium_malloc(size))),
            _size(size)
        {
            if(nullptr == _data)
            {
                throw exception_type(fmt::format("Failed to allocate {} bytes", size));
            }
            lock();
            _control_block = new control_block_type(*this);
        }

        secure_data_type(buffer_type* data, size_t size):
            secure_data_type(size)
        {
            unlock();
            memcpy(_data, data, size);
            sodium_memzero(data, size);
            lock();
        }

        secure_data_type(const secure_data_type& other):
            secure_data_type(other._size)
        {
            unlock();
            {
                reference_type reference = other.get_reference();

                memcpy(_data, reference->get_data(), reference->get_size());
            }
            lock();
        }

        secure_data_type(secure_data_type&& other) noexcept:
            _data(other._data),
            _size(other._size),
            _control_block(other._control_block),
            _locked(other._locked)
        {
            other._data = nullptr;
            other._control_block = nullptr;
            other._size = 0;

            if(nullptr != _control_block)
            {
                _control_block->_data = this;
            }
        }

        ~secure_data_type()
        {
            cleanup();
        }

        secure_data_type& operator=(const secure_data_type& other)
        {
            if(this == &other)
            {
                return *this;
            }

            cleanup();

            _data = static_cast<buffer_type*>(sodium_malloc(other._size));
            _size = other._size;
            if(nullptr == _data)
            {
                throw exception_type(fmt::format("Failed to allocate {} bytes", other._size));
            }

            {
                reference_type reference = other.get_reference();

                memcpy(_data, reference->get_data(), reference->get_size());
            }
            lock();
            _control_block = new control_block_type(*this);

            return *this;
        }

        secure_data_type& operator=(secure_data_type&& other) noexcept
        {
            if(this == &other)
            {
                return *this;
            }

            cleanup();

            _data = other._data;
            _size = other._size;
            _control_block = other._control_block;
            _locked = other._locked;
            other._data = nullptr;
            other._size = 0;
            other._control_block = nullptr;
            if(nullptr != _control_block)
            {
                _control_block->_data = this;
            }

            return *this;
        }

        [[nodiscard]] reference_type get_reference() const
        {
            return reference_type(*this);
        }

        [[nodiscard]] buffer_type* get_data()
        {
            return _data;
        }

        [[nodiscard]] const buffer_type* get_data() const
        {
            return _data;
        }

        [[nodiscard]] size_t get_size() const
        {
            return _size;
        }

        [[nodiscard]] bool is_locked() const
        {
            return _locked;
        }

    private:
        class control_block_type
        {
        public:
            explicit control_block_type(secure_data_type& data):
                _data(&data)
            {
                // Nothing to do yet
            }

            void increment()
            {
                ++_counter;
                if(1 == _counter && !_destroyed)
                {
                    _data->unlock();
                }
            }

            void decrement() noexcept
            {
                if(0 == _counter)
                {
                    spdlog::error("Internal error! Counter is already zero!");
                }
                else
                {
                    --_counter;
                    if(0 == _counter && !_destroyed)
                    {
                        _data->lock();
                    }
                    cleanup();
                }
            }

            void destroy() noexcept
            {
                _destroyed = true;
                _data = nullptr;
                cleanup();
            }
        private:
            friend class reference_type;
            friend class secure_data_type;

            void cleanup() const noexcept
            {
                if(_destroyed && 0 == _counter)
                {
                    delete this;
                }
            }

            secure_data_type* _data = nullptr;
            uint32_t _counter = 0;
            bool _destroyed = false;
        };

        void cleanup() noexcept
        {
            if(nullptr != _control_block)
            {
                _control_block->destroy();
            }
            _control_block = nullptr;
            sodium_free(_data);
            _data = nullptr;
            _size = 0;
        }

        void lock() noexcept
        {
            sodium_mprotect_noaccess(_data);
            _locked = true;
        }

        // TODO: Separate unlock for read and read/write based on constness of the reference_type

        void unlock()
        {
            sodium_mprotect_readwrite(_data);
            _locked = false;
        }

        buffer_type* _data = nullptr;
        size_t _size = 0;
        control_block_type* _control_block = nullptr;
        bool _locked = true;
    };
}
