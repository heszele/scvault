#include <scvault/shared_file.hh>
#include <scvault/exception.hh>

#include <fmt/format.h>

namespace scvault
{
    result_type<> shared_file_type::initialize(const std::filesystem::path& path,
                                               const crypto::public_key_type& owner_public_key,
                                               const crypto::secret_key_type& owner_secret_key)
    {
        binary_ofstream_type stream(path);

        if(!stream)
        {
            return tl::make_unexpected(fmt::format("Failed to open file at {}", path.string()));
        }

        //
        // Generate key for file encryption
        //

        crypto::key_type file_key;
        crypto_secretstream_xchacha20poly1305_keygen(file_key.data());

        //
        // Encrypt file key with public key
        //

        result_type<encrypt_key_result_type> encrypted_file_key_result = encrypt_key(file_key,
                                                                                     owner_public_key,
                                                                                     owner_secret_key);

        if(!encrypted_file_key_result)
        {
            return tl::make_unexpected(encrypted_file_key_result.error());
        }

        //
        // Write footer
        //

        const std::vector<footer_type> footers = {
            {
                encrypted_file_key_result->_nonce,
                owner_public_key,
                owner_public_key,
                encrypted_file_key_result->_encrypted_key
            }
        };

        write_footers(stream, footers);

        return {};
    }

    shared_file_type::shared_file_type(std::filesystem::path path,
                                       crypto::public_key_type public_key,
                                       crypto::secret_key_type secret_key):
        _path(std::move(path)),
        _public_key(std::move(public_key)),
        _secret_key(std::move(secret_key))
    {
        binary_ifstream_type stream(_path, std::ios_base::ate);

        if(!stream)
        {
            throw exception_type(fmt::format("Failed to open file at {}", _path.string()));
        }

        const size_t file_size = stream.tellg();

        if(file_size < sizeof(footer_type))
        {
            throw exception_type(fmt::format("Invalid file! Minimum size is {}", sizeof(footer_type)));
        }

        // Read the number of footers in the file, which is stored in the last four byte in big endian format
        byte_type number_of_footers_buffer[sizeof(uint32_t)] = { 0 };

        stream.seekg(-static_cast<int>(sizeof(uint32_t)), std::ios_base::cur);
        stream.read(number_of_footers_buffer, sizeof(uint32_t));

        const uint32_t number_of_footers = from_byte_stream(number_of_footers_buffer);

        if(number_of_footers * sizeof(footer_type) > file_size)
        {
            throw exception_type("Invalid file! Number of footer elements requires bigger file than it is");
        }

        // Read all the footer elements
        const int offset = static_cast<int>(sizeof(uint32_t) + number_of_footers * sizeof(footer_type));
        _footers.resize(number_of_footers);

        stream.seekg(-offset, std::ios_base::end);
        stream.read(reinterpret_cast<byte_type*>(_footers.data()), number_of_footers * sizeof(footer_type));

        if(const result_type<footer_type> result = get_footer(); !result)
        {
            throw exception_type(result.error());
        }

        _encrypted_content_size = file_size - number_of_footers * sizeof(footer_type) - sizeof(uint32_t);
    }

    size_t shared_file_type::get_encrypted_content_size() const
    {
        return _encrypted_content_size;
    }

    result_type<> shared_file_type::read(byte_type* data, size_t size) const
    {
        binary_ifstream_type input(_path);
        binary_ostringstream_type output;

        if(!input)
        {
            return tl::make_unexpected(fmt::format("Failed to open file at {} for reading", _path.string()));
        }

        result_type<crypto::key_type> file_key_result = get_file_key();

        if(!file_key_result)
        {
            return tl::make_unexpected(file_key_result.error());
        }

        const crypto::key_type file_key = *file_key_result;

        if(result_type<> result = crypto::decrypt(input, output, file_key); !result)
        {
            return result;
        }

        const size_t actual_size = std::min(size, output.data().size());

        std::copy_n(output.data().begin(), actual_size, data);

        return {};
    }

    result_type<> shared_file_type::write(const byte_type* data, size_t size)
    {
        binary_istringstream_type input(data, size);
        // TODO: Write to a temp file and only overwrite the original file at the end when everything went fine
        binary_ofstream_type output(_path, std::ios_base::trunc);

        if(!output)
        {
            return tl::make_unexpected(fmt::format("Failed to open file at {} for writing", _path.string()));
        }

        result_type<crypto::key_type> file_key_result = get_file_key();

        if(!file_key_result)
        {
            return tl::make_unexpected(file_key_result.error());
        }

        const crypto::key_type file_key = *file_key_result;

        if(result_type<> result = crypto::encrypt(input, output, file_key); !result)
        {
            return result;
        }

        _encrypted_content_size = static_cast<uint32_t>(output.tellp());

        write_footers(output);

        return {};
    }

    result_type<> shared_file_type::share(const crypto::public_key_type& public_key)
    {
        if(const result_type<footer_type> result = get_footer(public_key, _footers, _path); result)
        {
            return tl::make_unexpected(fmt::format("File at {} is already shared with this user", _path.string()));
        }

        if(!is_owner())
        {
            return tl::make_unexpected(fmt::format("Only the owner can share the file at {}", _path.string()));
        }

        result_type<crypto::key_type> file_key_result = get_file_key();

        if(!file_key_result)
        {
            return tl::make_unexpected(file_key_result.error());
        }

        crypto::key_type file_key = *file_key_result;

        result_type<encrypt_key_result_type> encrypted_file_key_result = encrypt_key(file_key,
                                                                                     public_key,
                                                                                     _secret_key);

        if(!encrypted_file_key_result)
        {
            return tl::make_unexpected(encrypted_file_key_result.error());
        }

        const footer_type footer = {
            encrypted_file_key_result->_nonce,
            public_key,
            _public_key,
            encrypted_file_key_result->_encrypted_key
        };

        _footers.emplace_back(footer);

        binary_ofstream_type output(_path, std::ios_base::app);

        if(!output)
        {
            tl::make_unexpected(fmt::format("Failed to open file at {} for writing", _path.string()));
        }

        output.seekp(_encrypted_content_size, std::ios_base::beg);
        write_footers(output);

        return {};
    }

    result_type<> shared_file_type::unshare(const crypto::public_key_type& public_key)
    {
        const result_type<footer_type> footer_result = get_footer(public_key, _footers, _path);

        if(!footer_result)
        {
            return tl::make_unexpected(fmt::format("File at {} is not shared with this user", _path.string()));
        }
        if(!is_owner())
        {
            return tl::make_unexpected(fmt::format("Only the owner can unshare the file at {}", _path.string()));
        }
        if(_public_key == public_key)
        {
            return tl::make_unexpected("The owners cannot unshare themselves");
        }

        const footer_type footer = *footer_result;

        const auto new_end = std::remove(_footers.begin(), _footers.end(), footer);

        if(new_end == _footers.end())
        {
            tl::make_unexpected("Internal error! Footer could not be removed");
        }

        _footers.erase(new_end, _footers.end());

        binary_ofstream_type output(_path, std::ios_base::app);

        if(!output)
        {
            tl::make_unexpected(fmt::format("Failed to open file at {} for writing", _path.string()));
        }

        output.seekp(_encrypted_content_size, std::ios_base::beg);
        write_footers(output);

        return {};
    }

    result_type<shared_file_type::encrypt_key_result_type> shared_file_type::encrypt_key(const crypto::key_type& key,
                                                                                         const crypto::public_key_type& public_key,
                                                                                         const crypto::secret_key_type& secret_key)
    {
        crypto::encrypted_key_type encrypted_key;
        crypto::nonce_type nonce;

        randombytes_buf(nonce.data(), nonce.size());

        // TODO: We should not use sodium functions directly from here
        if(crypto::secret_key_type::reference_type reference = secret_key.get_reference(); crypto_box_easy(encrypted_key.data(),
                                                                                                           key.data(),
                                                                                                           key.size(),
                                                                                                           nonce.data(),
                                                                                                           public_key.data(),
                                                                                                           reference->get_data()) != 0)
        {
            return tl::make_unexpected("Failed to encrypt key");
        }

        return { { nonce, encrypted_key } };
    }

    void shared_file_type::write_footers(binary_ostream_type& output, const std::vector<footer_type>& footers)
    {
        //
        // Write the footers themselves
        //

        output.write(reinterpret_cast<const byte_type*>(footers.data()), footers.size() * sizeof(footer_type));

        //
        // Write number of footers
        //

        const uint32_t number_of_footers = static_cast<uint32_t>(footers.size());
        byte_type number_of_footers_buffer[sizeof(uint32_t)] = { 0 };

        // The number is written in big endian format, so LSB first
        number_of_footers_buffer[0] = static_cast<byte_type>((number_of_footers & 0xff000000) >> 24);
        number_of_footers_buffer[1] = static_cast<byte_type>((number_of_footers & 0x00ff0000) >> 16);
        number_of_footers_buffer[2] = static_cast<byte_type>((number_of_footers & 0x0000ff00) >> 8);
        number_of_footers_buffer[3] = static_cast<byte_type>((number_of_footers & 0x000000ff) >> 0);

        output.write(number_of_footers_buffer, sizeof(uint32_t));
    }

    result_type<shared_file_type::footer_type> shared_file_type::get_footer(const crypto::public_key_type& public_key,
                                                                            const std::vector<footer_type>& footers,
                                                                            const std::filesystem::path& path)
    {
        auto predicate = [&public_key](const footer_type& footer)
        {
            return footer._public_key == public_key;
        };
        const auto footer = std::find_if(footers.begin(), footers.end(), predicate);

        // Check if the given key-pair can be used to open the file
        if(footer == footers.end())
        {
            return tl::make_unexpected(fmt::format("The file at {} cannot be opened with the given key-pair", path.string()));
        }

        return *footer;
    }

    void shared_file_type::write_footers(binary_ostream_type& output) const
    {
        write_footers(output, _footers);
    }

    result_type<crypto::key_type> shared_file_type::get_file_key() const
    {
        result_type<footer_type> result = get_footer();

        if(!result)
        {
            return tl::make_unexpected(result.error());
        }

        footer_type footer = *result;
        crypto::key_type file_key;

        if(crypto::secret_key_type::reference_type reference = _secret_key.get_reference(); crypto_box_open_easy(file_key.data(),
                                                                                                                 footer._encrypted_file_key.data(),
                                                                                                                 footer._encrypted_file_key.size(),
                                                                                                                 footer._nonce.data(),
                                                                                                                 footer._inviter_public_key.data(),
                                                                                                                 reference->get_data()) != 0)
        {
            return tl::make_unexpected("Failed to decrypt file key");
        }

        return { file_key };
    }

    result_type<shared_file_type::footer_type> shared_file_type::get_footer() const
    {
        return get_footer(_public_key, _footers, _path);
    }

    bool shared_file_type::is_owner() const
    {
        // The owner is the first entry in the footers
        return !_footers.empty() && _footers[0]._public_key == _public_key;
    }
}
