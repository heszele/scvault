#include <scvault/crypto.hh>
#include <scvault/exception.hh>

#include <fmt/format.h>

namespace scvault::crypto
{
    namespace
    {
        result_type<> check_streams(binary_istream_type& input, binary_ostream_type& output)
        {
            if(!input)
            {
                return tl::make_unexpected("Input is not valid");
            }
            if(!output)
            {
                return tl::make_unexpected("Output is not valid");
            }

            return {};
        }
    }

    result_type<> encrypt(binary_istream_type& input, binary_ostream_type& output,
                          const public_key_type& public_key, const secret_key_type& secret_key)
    {
        //
        // Validate inputs
        //

        if(result_type<> result = check_streams(input, output); !result)
        {
            return result;
        }

        //
        // Generate key for file encryption
        //

        key_type file_key;
        crypto_secretstream_xchacha20poly1305_keygen(file_key.data());

        //
        // Encrypt file key with public key
        //

        encrypted_key_type encrypted_file_key;
        nonce_type nonce;

        randombytes_buf(nonce.data(), nonce.size());

        if(secret_key_type::reference_type reference = secret_key.get_reference(); crypto_box_easy(encrypted_file_key.data(),
                                                                                                   file_key.data(),
                                                                                                   file_key.size(),
                                                                                                   nonce.data(),
                                                                                                   public_key.data(),
                                                                                                   reference->get_data()) != 0)
        {
            return tl::make_unexpected("Failed to encrypt file key");
        }

        //
        // Write encoded file key to file
        //

        output.write(nonce.data(), nonce.size());
        output.write(encrypted_file_key.data(), encrypted_file_key.size());

        //
        // Encode input file with file key
        //

        return encrypt(input, output, file_key);
    }

    result_type<> encrypt(binary_istream_type& input, binary_ostream_type& output, const key_type& file_key)
    {
        crypto_secretstream_xchacha20poly1305_state state;
        header_type header;

        if (crypto_secretstream_xchacha20poly1305_init_push(&state, header.data(), file_key.data()) != 0)
        {
            return tl::make_unexpected("Failed to initialize encryption stream");
        }

        output.write(header.data(), header.size());

        while (!input.eof())
        {
            array_type<256> buffer = { 0 };
            array_type<256 + crypto_secretstream_xchacha20poly1305_ABYTES> encrypted_buffer = { 0 };
            unsigned long long encrypted_buffer_length = 0;

            if(input.read(buffer.data(), buffer.size()).fail())
            {
                return tl::make_unexpected("Failed to read data from input stream");
            }

            // First encrypt the number of bytes written to the next block
            byte_type written_bytes[sizeof(uint32_t)] = { 0 };
            to_byte_stream(static_cast<uint32_t>(input.gcount()), written_bytes);

            if (crypto_secretstream_xchacha20poly1305_push(&state,
                                                           encrypted_buffer.data(), &encrypted_buffer_length,
                                                           written_bytes, sizeof(uint32_t),
                                                           nullptr, 0,
                                                           0) != 0)
            {
                return tl::make_unexpected("Failed to encrypt data");
            }

            output.write(encrypted_buffer.data(), encrypted_buffer_length);

            // Then encrypt the data itself
            const byte_type tag = input.eof() ? crypto_secretstream_xchacha20poly1305_TAG_FINAL : 0;
            if (crypto_secretstream_xchacha20poly1305_push(&state,
                                                           encrypted_buffer.data(), &encrypted_buffer_length,
                                                           buffer.data(), input.gcount(),
                                                           nullptr, 0,
                                                           tag) != 0)
            {
                return tl::make_unexpected("Failed to encrypt data");
            }

            output.write(encrypted_buffer.data(), encrypted_buffer_length);
        }

        return {};
    }

    result_type<> decrypt(binary_istream_type& input, binary_ostream_type& output,
                          const public_key_type& public_key, const secret_key_type& secret_key)
    {
        //
        // Validate inputs
        //

        if(result_type<> result = check_streams(input, output); !result)
        {
            return result;
        }

        key_type file_key;

        //
        // Read nonce and file key - file key is encrypted
        //

        encrypted_key_type encrypted_file_key = { 0 };
        nonce_type nonce;

        input.read(nonce.data(), nonce.size());
        input.read(encrypted_file_key.data(), encrypted_file_key.size());

        //
        // Decrypt file key
        //

        if(secret_key_type::reference_type secret_key_reference = secret_key.get_reference(); crypto_box_open_easy(file_key.data(),
                                                                                                                   encrypted_file_key.data(),
                                                                                                                   encrypted_file_key.size(),
                                                                                                                   nonce.data(),
                                                                                                                   public_key.data(),
                                                                                                                   secret_key_reference->get_data()) != 0)
        {
            return tl::make_unexpected("Failed to decrypt file key");
        }

        //
        // Decrypt file with file key
        //

        return decrypt(input, output, file_key);
    }

    result_type<> decrypt(binary_istream_type& input, binary_ostream_type& output, const key_type& file_key)
    {
        crypto_secretstream_xchacha20poly1305_state state;
        header_type header;

        input.read(header.data(), header.size());

        if(crypto_secretstream_xchacha20poly1305_init_pull(&state, header.data(), file_key.data()) != 0)
        {
            return tl::make_unexpected("Failed to initialize decryption stream");
        }

        while(!input.eof())
        {
            byte_type buffer[256] = { 0 };
            unsigned long long buffer_length = 0;
            array_type<256 + crypto_secretstream_xchacha20poly1305_ABYTES> encrypted_buffer = { 0 };
            byte_type tag = 0;

            // Read and decrypt the number of bytes of the next block
            input.read(encrypted_buffer.data(), sizeof(uint32_t) + crypto_secretstream_xchacha20poly1305_ABYTES);

            if(crypto_secretstream_xchacha20poly1305_pull(&state,
                                                          buffer, &buffer_length,
                                                          &tag,
                                                          encrypted_buffer.data(), input.gcount(),
                                                          nullptr, 0) != 0)
            {
                return tl::make_unexpected("Failed to decrypt data");
            }

            const uint32_t number_of_bytes_to_read = from_byte_stream(buffer) + crypto_secretstream_xchacha20poly1305_ABYTES;

            if(number_of_bytes_to_read > encrypted_buffer.size())
            {
                return tl::make_unexpected("Block is too big to read!");
            }

            // Read the next block and decrypt it
            input.read(encrypted_buffer.data(), number_of_bytes_to_read);

            if(crypto_secretstream_xchacha20poly1305_pull(&state,
                                                          buffer, &buffer_length,
                                                          &tag,
                                                          encrypted_buffer.data(), input.gcount(),
                                                          nullptr, 0) != 0)
            {
                return tl::make_unexpected("Failed to decrypt data");
            }

            output.write(buffer, buffer_length);

            if(tag == crypto_secretstream_xchacha20poly1305_TAG_FINAL)
            {
                break;
            }
        }

        return {};
    }

    std::string obfuscate(const std::string& string)
    {
        hash_type hash;

        if(-1 == crypto_generichash(hash.data(), hash.size(),
                                    reinterpret_cast<const unsigned char*>(string.c_str()), string.size(),
                                    nullptr, 0))
        {
            throw exception_type(fmt::format("Failed to generate hash for {}", string));
        }

        return to_base64(hash);
    }
}
