#include <scvault/stream.hh>

#include <algorithm>

namespace scvault
{
    uint32_t from_byte_stream(const byte_type* data)
    {
        return static_cast<uint32_t>(data[0]) << 24 |
               static_cast<uint32_t>(data[1]) << 16 |
               static_cast<uint32_t>(data[2]) <<  8 |
               static_cast<uint32_t>(data[3]) <<  0;
    }

    void to_byte_stream(uint32_t number, byte_type data[4])
    {
        data[0] = static_cast<byte_type>((number & 0xff000000) >> 24);
        data[1] = static_cast<byte_type>((number & 0x00ff0000) >> 16);
        data[2] = static_cast<byte_type>((number & 0x0000ff00) >> 8);
        data[3] = static_cast<byte_type>((number & 0x000000ff) >> 0);
    }

    //
    // binary_ifstream_type
    //

    binary_ifstream_type::binary_ifstream_type(const std::filesystem::path& path,
                                               std::ios_base::openmode open_mode):
        binary_ifstream_type(path.string(), open_mode)
    {
        // Nothing to do yet
    }

    binary_ifstream_type::binary_ifstream_type(const std::string& path,
                                               std::ios_base::openmode open_mode):
        _stream(path, open_mode | std::ios_base::binary)
    {
        if (!_stream)
        {
            // TODO: Error handling
        }
    }

    binary_ifstream_type& binary_ifstream_type::read(byte_type* data, size_t size)
    {
        _stream.read(reinterpret_cast<char*>(data), size);

        return *this;
    }

    bool binary_ifstream_type::to_bool() const
    {
        return static_cast<bool>(_stream);
    }

    bool binary_ifstream_type::eof() const
    {
        return _stream.eof();
    }

    std::streamsize binary_ifstream_type::gcount() const
    {
        return _stream.gcount();
    }

    bool binary_ifstream_type::fail() const
    {
        return _stream.fail();
    }

    size_t binary_ifstream_type::tellg()
    {
        return _stream.tellg();
    }

    void binary_ifstream_type::seekg(int32_t off, std::ios_base::seekdir dir)
    {
        _stream.seekg(off, dir);
    }

    void binary_ifstream_type::close()
    {
        _stream.close();
    }

    //
    // binary_istringstream_type
    //

    binary_istringstream_type::binary_istringstream_type(const byte_type* data, size_t size):
        _data(data, data + size),
        _position(_data.begin())
    {
        // Nothing to do yet
    }

    binary_istringstream_type& binary_istringstream_type::read(byte_type* data, size_t size)
    {
        const size_t remaining_size = _data.end() - _position;
        const size_t actual_size = std::min(remaining_size, size);

        std::copy_n(_position, actual_size, data);

        _position += actual_size;
        _gcount = actual_size;

        return *this;
    }

    bool binary_istringstream_type::to_bool() const
    {
        // TODO
        return true;
    }

    bool binary_istringstream_type::eof() const
    {
        return _position == _data.end();
    }

    std::streamsize binary_istringstream_type::gcount() const
    {
        return _gcount;
    }

    bool binary_istringstream_type::fail() const
    {
        // TODO
        return false;
    }

    //
    // binary_ofstream_type
    //

    binary_ofstream_type::binary_ofstream_type(const std::filesystem::path& path, std::ios_base::openmode open_mode):
        binary_ofstream_type(path.string(), open_mode)
    {
        // Nothing to do yet
    }

    binary_ofstream_type::binary_ofstream_type(const std::string& path, std::ios_base::openmode open_mode):
        _stream(path, open_mode | std::ios_base::binary)
    {
        if (!_stream)
        {
            // TODO: Error handling
        }
    }

    binary_ofstream_type& binary_ofstream_type::write(const byte_type* data, size_t size)
    {
        _stream.write(reinterpret_cast<const char*>(data), size);

        return *this;
    }

    bool binary_ofstream_type::to_bool() const
    {
        return static_cast<bool>(_stream);
    }

    bool binary_ofstream_type::eof() const
    {
        return _stream.eof();
    }

    bool binary_ofstream_type::fail() const
    {
        return _stream.fail();
    }

    //
    // binary_ostringstream_type
    //

    binary_ostringstream_type& binary_ostringstream_type::write(const byte_type* data, size_t size)
    {
        _data.insert(_data.end(), data, data + size);

        return *this;
    }

    bool binary_ostringstream_type::to_bool() const
    {
        // TODO
        return true;
    }

    bool binary_ostringstream_type::eof() const
    {
        // TODO
        return false;
    }

    bool binary_ostringstream_type::fail() const
    {
        // TODO
        return false;
    }

    const std::vector<byte_type>& binary_ostringstream_type::data() const
    {
        return _data;
    }
}
