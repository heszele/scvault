#include <scvault/vault.hh>

#include <scvault/crypto.hh>
#include <scvault/stream.hh>
#include <scvault/types.hh>
#include <scvault/shared_file.hh>

#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include <fstream>

namespace
{
    constexpr const char* VAULT_FILE = ".scvault";
}

namespace scvault
{
    credentials_type::credentials_type(std::string username, password_type password):
        _username(std::move(username)),
        _password(std::move(password))
    {
        // Nothing to do yet
    }

    const std::string& credentials_type::get_username() const
    {
        return _username;
    }

    crypto::secure_data_type<char>::reference_type credentials_type::get_password() const
    {
        return _password.get_reference();
    }

    bool vault_type::is_vault(const std::filesystem::path& path)
    {
        return check(path, false).has_value();
    }

    result_type<> vault_type::initialize(const std::filesystem::path& path, const credentials_type& admin)
    {
        // Check if the given path can be initialized as a vault
        if(result_type<> check_result = check(path, true); !check_result)
        {
            return check_result;
        }

        // Create vault file
        if(const std::ofstream vault_file(path / VAULT_FILE, std::ios_base::binary); !vault_file)
        {
            return tl::make_unexpected("Failed to create vault file");
        }

        // Create folder for user
        const std::string obfuscated_username = crypto::obfuscate(admin.get_username());

        if(std::filesystem::exists(path / obfuscated_username))
        {
            return tl::make_unexpected("The user already exists");
        }

        if(!std::filesystem::create_directory(path / obfuscated_username))
        {
            return tl::make_unexpected("Failed to create user");
        }

        // Create key pair from password
        crypto::salt_type salt;
        crypto::public_key_type public_key;
        crypto::secret_key_type secret_key;

        randombytes_buf(salt.data(), salt.size());
        if(result_type<> result = generate_key_pair(admin, salt, public_key, secret_key); !result)
        {
            return result;
        }

        // Store public key
        const std::string obfuscated_keystore_name = crypto::obfuscate("keystore");

        if(result_type<> result = shared_file_type::initialize(path / obfuscated_keystore_name, public_key, secret_key); !result)
        {
            return tl::make_unexpected(result.error());
        }

        const std::string public_key_base64 = crypto::to_base64(public_key);
        rapidjson::Document keystore_document;
        rapidjson::Document::AllocatorType& keystore_allocator = keystore_document.GetAllocator();
        rapidjson::Value keystore_key(admin.get_username().data(), static_cast<rapidjson::SizeType>(admin.get_username().size()));
        rapidjson::Value keystore_value(public_key_base64.data(), static_cast<rapidjson::SizeType>(public_key_base64.size()));

        keystore_document.SetObject();
        keystore_document.AddMember(keystore_key, keystore_value, keystore_allocator);

        const std::string keystore_json = to_string(keystore_document);

        shared_file_type shared_keystore(path / obfuscated_keystore_name, public_key, secret_key);

        if(result_type<> result = shared_keystore.write(reinterpret_cast<const byte_type*>(keystore_json.data()), keystore_json.size()); !result)
        {
            return tl::make_unexpected("Failed to write to the keystore file");
        }

        // Create user json
        rapidjson::Document document;
        rapidjson::Document::AllocatorType& allocator = document.GetAllocator();
        rapidjson::Value is_admin(true);
        rapidjson::Value collections(rapidjson::kArrayType);
        rapidjson::Value groups(rapidjson::kArrayType);

        document.SetObject();
        document.AddMember("is_admin", is_admin, allocator);
        document.AddMember("collections", collections, allocator);
        document.AddMember("groups", groups, allocator);

        const std::string user_json = to_string(document);

        // Encrypt user json
        binary_ofstream_type output(path / obfuscated_username / obfuscated_username);
        binary_istringstream_type input(reinterpret_cast<const byte_type*>(user_json.data()), user_json.size());

        if(!output)
        {
            return tl::make_unexpected("Failed to create user file");
        }

        output.write(salt.data(), salt.size());

        if(result_type<> result = crypto::encrypt(input, output, public_key, secret_key); !result)
        {
            return result;
        }

        return {};
    }

    vault_type::vault_type(std::filesystem::path path):
        _path(std::move(path))
    {
        // Check if the given path can be initialized as a vault
        if(const result_type<> check_result = check(_path, false); !check_result)
        {
            throw exception_type(fmt::format("Invalid vault path at {}", _path.string()));
        }
    }

    result_type<> vault_type::login(const credentials_type& credentials)
    {
        const std::string obfuscated_username = crypto::obfuscate(credentials.get_username());

        if(!std::filesystem::exists(_path / obfuscated_username))
        {
            return tl::make_unexpected(fmt::format("No such user with username '{}'", credentials.get_username()));
        }
        if(!std::filesystem::exists(_path / obfuscated_username / obfuscated_username))
        {
            return tl::make_unexpected("Internal error! User directory exists but no user file found");
        }

        // Decrypt user json
        binary_ifstream_type input(_path / obfuscated_username / obfuscated_username);
        binary_ostringstream_type output;

        if(!input)
        {
            return tl::make_unexpected("Failed to open user file for reading");
        }

        crypto::salt_type salt;

        input.read(salt.data(), salt.size());

        crypto::public_key_type public_key;
        crypto::secret_key_type secret_key;

        if(result_type<> result = generate_key_pair(credentials, salt, public_key, secret_key); !result)
        {
            return result;
        }

        if(result_type<> result = crypto::decrypt(input, output, public_key, secret_key); !result)
        {
            return result;
        }

        // TODO: Parse json
        // TODO: Create user data

        return {};
    }

    result_type<> vault_type::check(const std::filesystem::path& path, bool initialize)
    {
        const std::filesystem::directory_entry entry(path);

        // Sanity checks
        if(!entry.exists())
        {
            return tl::make_unexpected("The given path does not exist");
        }
        if(!entry.is_directory())
        {
            return tl::make_unexpected("This given path is not a directory");
        }
        if(initialize)
        {
            if(!std::filesystem::is_empty(entry))
            {
                return tl::make_unexpected("The directory must be empty");
            }
        }
        else
        {
            if(!std::filesystem::exists(path / VAULT_FILE))
            {
                return tl::make_unexpected("The given path is not a valid vault");
            }
        }

        return {};
    }

    std::string vault_type::to_string(const rapidjson::Document& document)
    {
        rapidjson::StringBuffer string_buffer;
        rapidjson::Writer writer(string_buffer);

        document.Accept(writer);

        return string_buffer.GetString();
    }

    result_type<> vault_type::generate_key_pair(const credentials_type& credentials,
                                                const crypto::salt_type& salt,
                                                crypto::public_key_type& public_key,
                                                crypto::secret_key_type& secret_key)
    {
        crypto::seed_type seed;

        if(crypto_pwhash(seed.data(), seed.size(),
                         credentials.get_password()->get_data(), credentials.get_password()->get_size(),
                         salt.data(),
#ifdef SCVAULT_DEBUG
                         crypto_pwhash_OPSLIMIT_INTERACTIVE,
                         crypto_pwhash_MEMLIMIT_INTERACTIVE,
#else
                         crypto_pwhash_OPSLIMIT_SENSITIVE,
                         crypto_pwhash_MEMLIMIT_SENSITIVE,
#endif
                         crypto_pwhash_ALG_DEFAULT) != 0)
        {
            return tl::make_unexpected("Failed to generate key pair");
        }

        if(crypto::secret_key_type::reference_type reference = secret_key.get_reference(); crypto_box_seed_keypair(public_key.data(),
                                                                                                                   reference->get_data(),
                                                                                                                   seed.data()) != 0)
        {
            return tl::make_unexpected("Failed to generate key pair");
        }

        return {};
    }
}
