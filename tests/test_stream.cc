#include <catch2/catch.hpp>

#include <scvault/types.hh>
#include <scvault/stream.hh>

#include <vector>

SCENARIO("scvault::binary_istringstream can easily be used", "[stream]")
{
    GIVEN("An in-memory buffer containing some data")
    {
        std::vector<scvault::byte_type> data = {
            0, 0, 0, 1,
            0, 0, 1, 0,
            0, 1, 0, 2
        };

        THEN("An scvault__binasry_istringstream can be created from it")
        {
            scvault::binary_istringstream_type input(data.data(), data.size());
            scvault::byte_type data_a[4] = { 0 };
            scvault::byte_type data_b[4] = { 0 };
            scvault::byte_type data_c[2] = { 0 };
            scvault::byte_type data_d[4] = { 0 };

            input.read(data_a, 4);

            REQUIRE_FALSE(input.eof());
            REQUIRE(input);
            REQUIRE_FALSE(input.fail());
            REQUIRE(input.gcount() == 4);

            input.read(data_b, 4);

            REQUIRE_FALSE(input.eof());
            REQUIRE(input);
            REQUIRE_FALSE(input.fail());
            REQUIRE(input.gcount() == 4);

            input.read(data_c, 2);

            REQUIRE_FALSE(input.eof());
            REQUIRE(input);
            REQUIRE_FALSE(input.fail());
            REQUIRE(input.gcount() == 2);

            // Try to read four bytes when there are only two left
            input.read(data_d, 4);

            CHECK(input.eof());
            REQUIRE(input);
            CHECK_FALSE(input.fail());
            REQUIRE(input.gcount() == 2);

            CHECK(data_a[0] == 0);
            CHECK(data_a[1] == 0);
            CHECK(data_a[2] == 0);
            CHECK(data_a[3] == 1);

            CHECK(data_b[0] == 0);
            CHECK(data_b[1] == 0);
            CHECK(data_b[2] == 1);
            CHECK(data_b[3] == 0);

            CHECK(data_c[0] == 0);
            CHECK(data_c[1] == 1);

            CHECK(data_d[0] == 0);
            CHECK(data_d[1] == 2);
            CHECK(data_d[2] == 0);
            CHECK(data_d[3] == 0);
        }
    }
}

SCENARIO("scvault::binary_ostringstream can easily be used", "[stream]")
{
    GIVEN("An scvault::binary_ostringstream object")
    {
        scvault::binary_ostringstream_type output;
        std::vector<scvault::byte_type> data = {
            0, 0, 0, 1,
            0, 0, 1, 0,
            0, 1, 0, 2
        };

        THEN("Data can easily be written to it")
        {
            output.write(data.data(), 4);

            REQUIRE(output);
            REQUIRE_FALSE(output.fail());
            REQUIRE_FALSE(output.eof());

            output.write(data.data() + 4, 4);

            REQUIRE(output);
            REQUIRE_FALSE(output.fail());
            REQUIRE_FALSE(output.eof());

            output.write(data.data() + 8, 2);

            REQUIRE(output);
            REQUIRE_FALSE(output.fail());
            REQUIRE_FALSE(output.eof());

            output.write(data.data() + 10, 2);

            REQUIRE(output);
            REQUIRE_FALSE(output.fail());
            REQUIRE_FALSE(output.eof());

            REQUIRE(data == output.data());
        }
    }
}