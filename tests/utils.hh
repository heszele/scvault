#pragma once
#include <filesystem>
#include <random>
#include <string>

#include <scvault/crypto.hh>

std::string generate_random_string();

[[nodiscard]] bool generate_key_pair(scvault::crypto::public_key_type& public_key,
                                     scvault::crypto::secret_key_type& secret_key);

class temp_directory_type
{
public:
    temp_directory_type();
    temp_directory_type(const temp_directory_type&) = delete;
    temp_directory_type(temp_directory_type&&) = delete;

    ~temp_directory_type();

    temp_directory_type& operator=(const temp_directory_type&) = delete;
    temp_directory_type& operator=(temp_directory_type&&) = delete;

    [[nodiscard]] const std::filesystem::path& get_path() const;
    [[nodiscard]] bool is_success() const;

private:
    std::filesystem::path _path;
    bool _success = false;
};
