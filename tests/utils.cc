#include "utils.hh"

std::string generate_random_string()
{
    std::string alphabet("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");

    std::random_device random_device;
    std::mt19937 generator(random_device());

    std::shuffle(alphabet.begin(), alphabet.end(), generator);

    return alphabet.substr(0, 32);
}

bool generate_key_pair(scvault::crypto::public_key_type& public_key, scvault::crypto::secret_key_type& secret_key)
{
    scvault::crypto::secret_key_type::reference_type secret_key_reference = secret_key.get_reference();

    if (crypto_box_keypair(public_key.data(), secret_key_reference->get_data()) != 0)
    {
        return false;
    }

    return true;
}

temp_directory_type::temp_directory_type():
    _path(std::filesystem::temp_directory_path() / generate_random_string())
{
    if (std::filesystem::create_directory(_path))
    {
        _success = true;
    }
}

temp_directory_type::~temp_directory_type()
{
    std::error_code error_code;

    std::filesystem::remove_all(_path, error_code);
}

const std::filesystem::path& temp_directory_type::get_path() const
{
    return _path;
}

bool temp_directory_type::is_success() const
{
    return _success;
}
