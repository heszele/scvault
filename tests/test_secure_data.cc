#include <catch2/catch.hpp>

#include <scvault/secure_data.hh>

#include <memory>

SCENARIO("scvault::secure_data_type can be easily used", "[secure-data]")
{
    // TODO: How to handle sodium_init?
    REQUIRE(sodium_init() >= 0);

    GIVEN("A char array with some data")
    {
        char data[] = {
            'a', 'b', 'c', 'd'
        };

        THEN("It can be turned into an scvault::secure_data_type")
        {
            auto secure_data = std::make_shared<scvault::crypto::secure_data_type<char>>(data, 4);
            char cleared_data[] = {
                0, 0, 0, 0
            };

            REQUIRE(sodium_memcmp(data, cleared_data, 4) == 0);
            REQUIRE(secure_data->is_locked());

            AND_THEN("The secure data can only be used through references")
            {
                {
                    scvault::crypto::secure_data_type<char>::reference_type reference = secure_data->get_reference();
                    char expected_data[] = {
                        'a', 'b', 'c', 'd'
                    };

                    REQUIRE_FALSE(secure_data->is_locked());
                    REQUIRE(reference.is_valid());
                    REQUIRE(4 == reference->get_size());
                    REQUIRE(sodium_memcmp(expected_data, reference->get_data(), reference->get_size()) == 0);

                    // Do some changes and then forget about the reference
                    reference->get_data()[2] = 'z';
                }
                REQUIRE(secure_data->is_locked());
                {
                    // The changes are visible in a new reference
                    scvault::crypto::secure_data_type<char>::reference_type reference = secure_data->get_reference();
                    char expected_data[] = {
                        'a', 'b', 'z', 'd'
                    };

                    REQUIRE_FALSE(secure_data->is_locked());
                    REQUIRE(reference.is_valid());
                    REQUIRE(4 == reference->get_size());
                    REQUIRE(sodium_memcmp(expected_data, reference->get_data(), reference->get_size()) == 0);

                    AND_THEN("The reference can be released to lock the data")
                    {
                        reference.release();

                        REQUIRE(secure_data->is_locked());
                        REQUIRE_FALSE(reference.is_valid());
                    }

                    AND_THEN("If the secure data is deleted, the reference will be invalidated")
                    {
                        secure_data.reset();

                        REQUIRE_FALSE(reference.is_valid());
                    }
                }
            }
        }
    }
}

SCENARIO("scvault::secure_data_type can be copied", "[secure-data]")
{
    REQUIRE(sodium_init() >= 0);

    GIVEN("A char array with some data")
    {
        char data[] = {
            'a', 'b', 'c', 'd'
        };

        THEN("It can be turned into an scvault::secure_data_type")
        {
            scvault::crypto::secure_data_type secure_data_a(data, 4);
            char cleared_data[] = {
                0, 0, 0, 0
            };

            REQUIRE(sodium_memcmp(data, cleared_data, 4) == 0);
            REQUIRE(secure_data_a.is_locked());

            AND_THEN("A reference_type obejct can be aquired to read or write the data")
            {
                scvault::crypto::secure_data_type<char>::reference_type reference_a = secure_data_a.get_reference();

                REQUIRE_FALSE(secure_data_a.is_locked());

                reference_a->get_data()[2] = 'z';

                AND_THEN("It can be copied to a new object")
                {
                    scvault::crypto::secure_data_type<char> secure_data_b = secure_data_a;
                    char copied_data[] = {
                        'a', 'b', 'z', 'd'
                    };

                    REQUIRE_FALSE(secure_data_a.is_locked());

                    scvault::crypto::secure_data_type<char>::reference_type reference_b = secure_data_b.get_reference();

                    REQUIRE_FALSE(secure_data_b.is_locked());
                    REQUIRE(sodium_memcmp(copied_data, reference_b->get_data(), reference_b->get_size()) == 0);
                    REQUIRE(reference_a->get_size() == reference_b->get_size());
                    REQUIRE(sodium_memcmp(reference_a->get_data(), reference_b->get_data(), reference_b->get_size()) == 0);

                    AND_THEN("The copied object has its own data independent from the copied one")
                    {
                        reference_b->get_data()[1] = 'x';
                        char changed_data[] = {
                            'a', 'x', 'z', 'd'
                        };

                        REQUIRE(sodium_memcmp(changed_data, reference_b->get_data(), reference_b->get_size()) == 0);
                        REQUIRE(sodium_memcmp(copied_data, reference_a->get_data(), reference_a->get_size()) == 0);
                    }
                }
                AND_THEN("It can be copied to an existing object")
                {
                    scvault::crypto::secure_data_type secure_data_b(data, 4);

                    secure_data_b = secure_data_a;
                    char copied_data[] = {
                        'a', 'b', 'z', 'd'
                    };

                    REQUIRE_FALSE(secure_data_a.is_locked());

                    scvault::crypto::secure_data_type<char>::reference_type reference_b = secure_data_b.get_reference();

                    REQUIRE_FALSE(secure_data_b.is_locked());
                    REQUIRE(sodium_memcmp(copied_data, reference_b->get_data(), reference_b->get_size()) == 0);
                    REQUIRE(reference_a->get_size() == reference_b->get_size());
                    REQUIRE(sodium_memcmp(reference_a->get_data(), reference_b->get_data(), reference_b->get_size()) == 0);

                    AND_THEN("The copied object has its own data independent from the copied one")
                    {
                        reference_b->get_data()[1] = 'x';
                        char changed_data[] = {
                            'a', 'x', 'z', 'd'
                        };

                        REQUIRE(sodium_memcmp(changed_data, reference_b->get_data(), reference_b->get_size()) == 0);
                        REQUIRE(sodium_memcmp(copied_data, reference_a->get_data(), reference_a->get_size()) == 0);
                    }
                }
            }
        }
    }
}

SCENARIO("scvault::secure_data_type can be moved", "[secure-data]")
{
    REQUIRE(sodium_init() >= 0);

    GIVEN("A char array with some data")
    {
        char data[] = {
            'a', 'b', 'c', 'd'
        };

        THEN("It can be turned into an scvault::secure_data_type")
        {
            scvault::crypto::secure_data_type secure_data_a(data, 4);
            char cleared_data[] = {
                0, 0, 0, 0
            };

            REQUIRE(sodium_memcmp(data, cleared_data, 4) == 0);
            REQUIRE(secure_data_a.is_locked());

            AND_THEN("A reference_type obejct can be aquired to read or write the data")
            {
                scvault::crypto::secure_data_type<char>::reference_type reference_a = secure_data_a.get_reference();

                REQUIRE_FALSE(secure_data_a.is_locked());
                REQUIRE(reference_a.is_valid());

                char* old_data = reference_a->get_data();

                reference_a->get_data()[2] = 'z';

                AND_THEN("It can be moved to a new object")
                {
                    scvault::crypto::secure_data_type<char> secure_data_b = std::move(secure_data_a);
                    char copied_data[] = {
                        'a', 'b', 'z', 'd'
                    };

                    REQUIRE_FALSE(secure_data_b.is_locked());
                    REQUIRE(reference_a.is_valid());
                    REQUIRE(sodium_memcmp(copied_data, reference_a->get_data(), reference_a->get_size()) == 0);

                    scvault::crypto::secure_data_type<char>::reference_type reference_b = secure_data_b.get_reference();

                    REQUIRE(reference_b.is_valid());
                    // Cast the pointers to void* to avoid Catch2 to convert the char* to string
                    REQUIRE(static_cast<void*>(reference_b->get_data()) == static_cast<void*>(old_data));
                }
                AND_THEN("It can be moved to an existing object")
                {
                    scvault::crypto::secure_data_type secure_data_b(data, 4);

                    secure_data_b = std::move(secure_data_a);
                    char copied_data[] = {
                        'a', 'b', 'z', 'd'
                    };

                    REQUIRE_FALSE(secure_data_b.is_locked());
                    REQUIRE(reference_a.is_valid());
                    REQUIRE(sodium_memcmp(copied_data, reference_a->get_data(), reference_a->get_size()) == 0);

                    scvault::crypto::secure_data_type<char>::reference_type reference_b = secure_data_b.get_reference();

                    REQUIRE(reference_b.is_valid());
                    // Cast the pointers to void* to avoid Catch2 to convert the char* to string
                    REQUIRE(static_cast<void*>(reference_b->get_data()) == static_cast<void*>(old_data));
                }
            }
        }
    }
}

SCENARIO("scvault::secure_data_type::reference_type can be released", "[secure-data]")
{
    REQUIRE(sodium_init() >= 0);

    GIVEN("A char array with some data")
    {
        char data[] = {
            'a', 'b', 'c', 'd'
        };

        THEN("It can be turned into an scvault::secure_data_type")
        {
            scvault::crypto::secure_data_type secure_data(data, 4);

            REQUIRE(secure_data.is_locked());

            AND_THEN("A reference_type obejct can be aquired to read or write the data")
            {
                scvault::crypto::secure_data_type<char>::reference_type reference = secure_data.get_reference();

                REQUIRE_FALSE(secure_data.is_locked());
                REQUIRE(reference.is_valid());

                AND_THEN("This reference can be released to lock the data again")
                {
                    reference.release();

                    REQUIRE(secure_data.is_locked());
                    REQUIRE_FALSE(reference.is_valid());
                }
            }
        }
    }
}

SCENARIO("scvault::secure_data_type::reference_type can be copied", "[secure-data]")
{
    REQUIRE(sodium_init() >= 0);

    GIVEN("A char array with some data")
    {
        char data[] = {
            'a', 'b', 'c', 'd'
        };

        THEN("It can be turned into an scvault::secure_data_type")
        {
            scvault::crypto::secure_data_type secure_data(data, 4);

            REQUIRE(secure_data.is_locked());

            AND_THEN("A reference_type obejct can be aquired to read or write the data")
            {
                scvault::crypto::secure_data_type<char>::reference_type reference_a = secure_data.get_reference();

                REQUIRE_FALSE(secure_data.is_locked());
                REQUIRE(reference_a.is_valid());

                AND_THEN("This reference can be copied to a new object")
                {
                    {
                        scvault::crypto::secure_data_type<char>::reference_type reference_b = reference_a;

                        REQUIRE_FALSE(secure_data.is_locked());
                        REQUIRE(reference_a.is_valid());
                        REQUIRE(reference_b.is_valid());
                        REQUIRE(static_cast<void*>(reference_a->get_data()) == static_cast<void*>(reference_b->get_data()));
                    }

                    // When the copied reference goes out of scope, the data remains unlocked
                    REQUIRE_FALSE(secure_data.is_locked());
                    REQUIRE(reference_a.is_valid());
                }
                AND_THEN("This reference can be copied to an existing object")
                {
                    {
                        scvault::crypto::secure_data_type<char>::reference_type reference_b = secure_data.get_reference();

                        reference_b = reference_a;
                        REQUIRE_FALSE(secure_data.is_locked());
                        REQUIRE(reference_a.is_valid());
                        REQUIRE(reference_b.is_valid());
                        REQUIRE(static_cast<void*>(reference_a->get_data()) == static_cast<void*>(reference_b->get_data()));
                    }

                    // When the copied reference goes out of scope, the data remains unlocked
                    REQUIRE_FALSE(secure_data.is_locked());
                    REQUIRE(reference_a.is_valid());
                }
            }
        }
    }
}

SCENARIO("scvault::secure_data_type::reference_type can be moved", "[secure-data]")
{
    REQUIRE(sodium_init() >= 0);

    GIVEN("A char array with some data")
    {
        char data[] = {
            'a', 'b', 'c', 'd'
        };

        THEN("It can be turned into an scvault::secure_data_type")
        {
            scvault::crypto::secure_data_type secure_data(data, 4);

            REQUIRE(secure_data.is_locked());

            AND_THEN("A reference_type obejct can be aquired to read or write the data")
            {
                scvault::crypto::secure_data_type<char>::reference_type reference_a = secure_data.get_reference();

                REQUIRE_FALSE(secure_data.is_locked());
                REQUIRE(reference_a.is_valid());

                char* old_data = reference_a->get_data();

                AND_THEN("This reference can be moved to a new object")
                {
                    {
                        scvault::crypto::secure_data_type<char>::reference_type reference_b = std::move(reference_a);

                        REQUIRE_FALSE(secure_data.is_locked());
                        REQUIRE(reference_b.is_valid());
                        REQUIRE(static_cast<void*>(old_data) == static_cast<void*>(reference_b->get_data()));
                    }

                    // Data should be locked, since the only reference to it went out of scope
                    REQUIRE(secure_data.is_locked());
                }
                AND_THEN("This reference can be moved to an existing object")
                {
                    {
                        scvault::crypto::secure_data_type<char>::reference_type reference_b = secure_data.get_reference();

                        reference_b = std::move(reference_a);
                        REQUIRE_FALSE(secure_data.is_locked());
                        REQUIRE(reference_b.is_valid());
                        REQUIRE(static_cast<void*>(old_data) == static_cast<void*>(reference_b->get_data()));
                    }

                    // Data should be locked, since the only reference to it went out of scope
                    REQUIRE(secure_data.is_locked());
                }
            }
        }
    }
}
