#include <catch2/catch.hpp>
#include "utils.hh"

#include <scvault/vault.hh>

#include <filesystem>
#include <fstream>
#include <memory>

SCENARIO("scvault::vault_type can be used to check if a directory is a valid vault or not", "[vault]")
{
    GIVEN("A non-existing directory")
    {
        const std::filesystem::path non_existing("some_random_non_existing_path");

        THEN("It cannot be used as a vault")
        {
            REQUIRE_FALSE(scvault::vault_type::is_vault(non_existing));
        }
    }
    GIVEN("An random temp directory")
    {
        temp_directory_type temp_directory;

        REQUIRE(temp_directory.is_success());

        THEN("It is not a vault by default")
        {
            REQUIRE_FALSE(scvault::vault_type::is_vault(temp_directory.get_path()));
        }
        AND_GIVEN("A file in that directory")
        {
            std::ofstream stream(temp_directory.get_path() / "file");

            REQUIRE(stream);
            stream.close();

            THEN("That file cannot be used as a vault")
            {
                REQUIRE_FALSE(scvault::vault_type::is_vault(temp_directory.get_path() / "file"));
            }
        }
        AND_GIVEN("A .scvault file in that directory")
        {
            std::ofstream stream(temp_directory.get_path() / ".scvault");

            REQUIRE(stream);
            stream.close();

            THEN("The directory is considered to be a vault")
            {
                REQUIRE(scvault::vault_type::is_vault(temp_directory.get_path()));
            }
        }
    }
}

SCENARIO("scvault::vault_type can be used to initialise a new vault", "[vault]")
{
    REQUIRE(sodium_init() >= 0);
    GIVEN("An empty directory")
    {
        temp_directory_type temp_directory;

        REQUIRE(temp_directory.is_success());
        AND_GIVEN("An admin user")
        {
            std::string admin_username("admin");
            std::string admin_password("admin");
            const scvault::credentials_type::password_type password(admin_password.data(), admin_password.size());
            scvault::credentials_type credentials(admin_username, password);

            AND_GIVEN("A file in that directory")
            {
                std::ofstream output(temp_directory.get_path() / "just_a_file");

                REQUIRE(output);

                output.close();

                THEN("The folder cannot be initialized as a vault, since it is not empty")
                {
                    scvault::result_type<> result = scvault::vault_type::initialize(temp_directory.get_path(), credentials);

                    REQUIRE_FALSE(result);
                    REQUIRE(result.error() == "The directory must be empty");
                }
            }
            THEN("The empty directory can be initialized as a vault")
            {
                REQUIRE(scvault::vault_type::initialize(temp_directory.get_path(), credentials));

                // The directory must not be empty
                REQUIRE_FALSE(std::filesystem::is_empty(temp_directory.get_path()));

                // Check the files in the directory
                uint32_t admin_directory_counter = 0;
                std::filesystem::directory_entry admin_entry;
                uint32_t vault_file_counter = 0;
                uint32_t keystore_file_counter = 0;
                std::filesystem::directory_entry keystore_entry;
                uint32_t counter = 0;

                for(const auto& entry: std::filesystem::directory_iterator(temp_directory.get_path()))
                {
                    if(entry.is_directory())
                    {
                        admin_entry = entry;
                        ++admin_directory_counter;
                    }
                    else
                    {
                        if(entry.path().filename().string() == ".scvault")
                        {
                            ++vault_file_counter;
                        }
                        else if(entry.path().filename().string() == "DIBy4W5Ky8PgBvvAnw_p6KxoYR9GYxt2ViTaFDHndlM=")
                        {
                            // We found the keystore file with its obfuscated name
                            ++keystore_file_counter;
                        }
                    }
                    ++counter;
                }

                REQUIRE(1 == admin_directory_counter);
                REQUIRE(1 == vault_file_counter);
                REQUIRE(1 == keystore_file_counter);
                REQUIRE(3 == counter);

                // Get the directory name of admin
                REQUIRE(admin_entry.path().has_filename());

                const std::string directory_name = admin_entry.path().filename().string();

                REQUIRE_FALSE(directory_name.empty());
                // The filename must be equal to the obfuscated value of "admin", the username
                REQUIRE(admin_entry.path().filename().string() == "GqZrOTuKtH0kid5DAW9fOcqMJBJzfGzcX3vDPeL22+0=");
                REQUIRE_FALSE(std::filesystem::is_empty(admin_entry.path()));

                uint32_t admin_file_counter = 0;

                counter = 0;
                for(const auto& entry: std::filesystem::directory_iterator(admin_entry.path()))
                {
                    if(!entry.is_directory() && entry.path().filename().string() == directory_name)
                    {
                        admin_entry = entry;
                        ++admin_file_counter;
                    }
                    ++counter;
                }

                REQUIRE(1 == admin_file_counter);
                REQUIRE(1 == counter);

                /*std::vector<std::string> path_parts;

                for(const auto& part: admin_entry.path())
                {
                    path_parts.push_back(part.string());
                }

                REQUIRE_FALSE(path_parts.empty());

                const std::string directory_name = path_parts.back();*/
            }
        }
    }
}

SCENARIO("scvault::vault_type object can be created with a valid vault path", "[vault]")
{
    REQUIRE(sodium_init() >= 0);
    GIVEN("An empty directory")
    {
        temp_directory_type temp_directory;

        REQUIRE(temp_directory.is_success());
        AND_GIVEN("An admin user")
        {
            std::string admin_username("admin");
            std::string admin_password("admin");
            const scvault::credentials_type::password_type password(admin_password.data(), admin_password.size());
            scvault::credentials_type credentials(admin_username, password);

            THEN("The empty directory can be initialized as a vault")
            {
                REQUIRE(scvault::vault_type::initialize(temp_directory.get_path(), credentials));

                AND_THEN("An scvault::vault_type object can be created with that path")
                {
                    REQUIRE_NOTHROW(scvault::vault_type(temp_directory.get_path()));
                }
            }
        }
        THEN("Creating an scvault::vault_type object with that empty directory will throw en axception")
        {
            REQUIRE_THROWS_AS(scvault::vault_type(temp_directory.get_path()), scvault::exception_type);
        }
    }
}

SCENARIO("scvault::vault_type object can be used to log in", "[vault]")
{
    REQUIRE(sodium_init() >= 0);
    GIVEN("An empty directory")
    {
        temp_directory_type temp_directory;

        REQUIRE(temp_directory.is_success());
        AND_GIVEN("An admin user")
        {
            std::string admin_username("admin");
            std::string admin_password("admin");
            const scvault::credentials_type::password_type password(admin_password.data(), admin_password.size());
            scvault::credentials_type credentials(admin_username, password);

            THEN("The empty directory can be initialized as a vault")
            {
                REQUIRE(scvault::vault_type::initialize(temp_directory.get_path(), credentials));

                AND_THEN("An scvault::vault_type object can be created with that path")
                {
                    std::unique_ptr<scvault::vault_type> vault;

                    REQUIRE_NOTHROW(vault = std::make_unique<scvault::vault_type>(temp_directory.get_path()));
                    AND_THEN("The admin user can log in")
                    {
                        REQUIRE(vault->login(credentials));
                    }
                    AND_THEN("Unknown users cannot log in")
                    {
                        std::string user_username("user");
                        std::string user_password("user");
                        scvault::credentials_type user_credentials(user_username,
                                                                   scvault::credentials_type::password_type(user_password.data(),
                                                                                                            user_password.size()));
                        scvault::result_type<> result = vault->login(user_credentials);

                        REQUIRE_FALSE(result);
                        REQUIRE(result.error() == "No such user with username 'user'");
                    }
                    AND_THEN("If the user file is missing the user cannot log in")
                    {
                        REQUIRE(std::filesystem::remove(temp_directory.get_path() /
                                                        "GqZrOTuKtH0kid5DAW9fOcqMJBJzfGzcX3vDPeL22+0=" /
                                                        "GqZrOTuKtH0kid5DAW9fOcqMJBJzfGzcX3vDPeL22+0="));
                        scvault::result_type<> result = vault->login(credentials);

                        REQUIRE_FALSE(result);
                        REQUIRE(result.error() == "Internal error! User directory exists but no user file found");
                    }
                }
            }
        }
    }
}
