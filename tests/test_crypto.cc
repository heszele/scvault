#include <catch2/catch.hpp>
#include "utils.hh"

#include <scvault/crypto.hh>
#include <scvault/types.hh>
#include <scvault/stream.hh>

SCENARIO("scvault::crypto::to_base64 works as intended", "[crypto]")
{
    GIVEN("A byte array")
    {
        const scvault::array_type<4> array = {
            'a', 'b', 'c', 'd'
        };
        THEN("It can easily be encoded in base64")
        {
            const std::string base64 = scvault::crypto::to_base64(array);

            REQUIRE(base64 == "YWJjZA==");
        }
    }
    GIVEN("A byte vector")
    {
        const scvault::vector_type vector = {
            'a', 'b', 'c', 'd'
        };
        THEN("It can easily be encoded in base64")
        {
            const std::string base64 = scvault::crypto::to_base64(vector);

            REQUIRE(base64 == "YWJjZA==");
        }
    }
    GIVEN("A byte array which would be encoded to base64 with a '/' character")
    {
        const scvault::vector_type vector = {
            255, 'b', 'c', 'd'
        };
        THEN("It can easily be encoded in base64 and the '/' character will be replaced with '_'")
        {
            const std::string base64 = scvault::crypto::to_base64(vector);

            REQUIRE(base64 == "_2JjZA==");
        }
    }
}

SCENARIO("scvault::crypto::obfuscate works as intended", "[crypto]")
{
    GIVEN("A string")
    {
        const std::string string("string");

        THEN("Its obfuscated value can easuly be created")
        {
            const std::string obfuscated_a = scvault::crypto::obfuscate(string);

            REQUIRE(obfuscated_a == "wiasiesyAPIP6NjFbIso8eUpqQSjUNzQE6e6ylwRjMk=");

            AND_THEN("For the same string, the obfuscated string will always be the same")
            {
                const std::string obfuscated_b = scvault::crypto::obfuscate(string);

                REQUIRE(obfuscated_a == obfuscated_b);
            }
        }
    }
}

SCENARIO("scvault::crypto can encrypt and decrypt data", "[crypto]")
{
    REQUIRE(sodium_init() >= 0);
    GIVEN("A key pair")
    {
        scvault::crypto::public_key_type public_key;
        scvault::crypto::secret_key_type secret_key;

        REQUIRE(generate_key_pair(public_key, secret_key));

        AND_GIVEN("Some data in memory")
        {
            const std::string data("This is a string to be encrypted");

            THEN("scvault::crypto can be used to encrypt it into a stream")
            {
                scvault::binary_istringstream_type input(reinterpret_cast<const scvault::byte_type*>(data.data()), data.size());
                scvault::binary_ostringstream_type encrypted_output;

                REQUIRE(scvault::crypto::encrypt(input, encrypted_output, public_key, secret_key));

                AND_THEN("The original data can be decrypted")
                {
                    scvault::binary_istringstream_type encrypted_input(encrypted_output.data().data(), encrypted_output.data().size());
                    scvault::binary_ostringstream_type output;

                    REQUIRE(scvault::crypto::decrypt(encrypted_input, output, public_key, secret_key));
                    REQUIRE(std::vector<scvault::byte_type>(data.data(), data.data() + data.size()) == output.data());
                }
            }
        }
    }
}

SCENARIO("scvault::crypto can detect errors during encryption and decryption" "[crypto]")
{
    REQUIRE(sodium_init() >= 0);
    GIVEN("A key pair")
    {
        scvault::crypto::public_key_type public_key;
        scvault::crypto::secret_key_type secret_key;

        REQUIRE(generate_key_pair(public_key, secret_key));

        AND_GIVEN("An invalid input stream")
        {
            scvault::binary_ifstream_type input(std::string("non_existing_directory/non_existing_file"));
            scvault::binary_ostringstream_type output;

            THEN("Encrypt will fail")
            {
                scvault::result_type<> result = scvault::crypto::encrypt(input, output, public_key, secret_key);

                REQUIRE_FALSE(result);
                REQUIRE(result.error() == "Input is not valid");
            }
            THEN("Decrypt will fail")
            {
                scvault::result_type<> result = scvault::crypto::decrypt(input, output, public_key, secret_key);

                REQUIRE_FALSE(result);
                REQUIRE(result.error() == "Input is not valid");
            }
        }
        AND_GIVEN("An invalid output stream")
        {
            scvault::binary_ifstream_type input;
            scvault::binary_ofstream_type output(std::string("non_existing_directory/non_existing_file"));

            THEN("Encrypt will fail")
            {
                scvault::result_type<> result = scvault::crypto::encrypt(input, output, public_key, secret_key);

                REQUIRE_FALSE(result);
                REQUIRE(result.error() == "Output is not valid");
            }
            THEN("Decrypt will fail")
            {
                scvault::result_type<> result = scvault::crypto::decrypt(input, output, public_key, secret_key);

                REQUIRE_FALSE(result);
                REQUIRE(result.error() == "Output is not valid");
            }
        }
        AND_GIVEN("Some data in memory")
        {
            const std::string data("This is a string to be encrypted");

            THEN("scvault::crypto can be used to encrypt it into a stream")
            {
                scvault::binary_istringstream_type input(reinterpret_cast<const scvault::byte_type*>(data.data()), data.size());
                scvault::binary_ostringstream_type encrypted_output;

                REQUIRE(scvault::crypto::encrypt(input, encrypted_output, public_key, secret_key));

                AND_THEN("Using the wrong keys, the descryption will fail")
                {
                    scvault::binary_istringstream_type encrypted_input(encrypted_output.data().data(), encrypted_output.data().size());
                    scvault::binary_ostringstream_type output;
                    scvault::result_type<> result = scvault::crypto::decrypt(encrypted_input, output, scvault::crypto::public_key_type(), secret_key);

                    REQUIRE_FALSE(result);
                    REQUIRE(result.error() == "Failed to decrypt file key");
                }
            }
        }
    }
}
