#include <catch2/catch.hpp>
#include "utils.hh"

#include <scvault/shared_file.hh>
#include <scvault/types.hh>
#include <scvault/stream.hh>

SCENARIO("scvault::shared_file_type can easily be used", "[shared_file]")
{
    REQUIRE(sodium_init() >= 0);
    GIVEN("A key pair")
    {
        scvault::crypto::public_key_type public_key;
        scvault::crypto::secret_key_type secret_key;

        REQUIRE(generate_key_pair(public_key, secret_key));

        THEN("It can be used to generate a sharable file")
        {
            const temp_directory_type temp_directory;

            REQUIRE(temp_directory.is_success());

            std::filesystem::path path = temp_directory.get_path() / "shared_file";

            REQUIRE(scvault::shared_file_type::initialize(path, public_key, secret_key));

            scvault::binary_ifstream_type input(path, std::ios_base::ate);

            REQUIRE(input);

            const size_t file_size = input.tellg();

            REQUIRE(file_size == 140); // footer is 136 byte + 4 byte for the number of footers

            scvault::byte_type buffer[4] = { 0 };

            input.seekg(-4, std::ios_base::end);
            input.read(buffer, 4);

            // Number of footers is written in big endian format
            REQUIRE(buffer[0] == 0);
            REQUIRE(buffer[1] == 0);
            REQUIRE(buffer[2] == 0);
            REQUIRE(buffer[3] == 1);

            input.close();

            AND_THEN("This shared file can easily be opened")
            {
                scvault::shared_file_type shared_file(path, public_key, secret_key);

                REQUIRE(shared_file.get_encrypted_content_size() == 0);

                AND_THEN("Data can be written to it")
                {
                    std::vector<scvault::byte_type> data = {
                        0, 1, 2, 3, 4, 5, 6, 7, 8, 9
                    };

                    REQUIRE(shared_file.write(data.data(), data.size()));
                    // 24 bytes for the header
                    // 4 + 17 bytes for the encrypted block size
                    // 10 + 17 bytes for the encrypted data
                    REQUIRE(shared_file.get_encrypted_content_size() == 72);

                    AND_THEN("The data can be read back")
                    {
                        std::vector<scvault::byte_type> read_data(10);

                        REQUIRE(shared_file.read(read_data.data(), read_data.size()));
                        REQUIRE(data == read_data);
                    }
                    AND_THEN("The file can be easily shared")
                    {
                        scvault::crypto::public_key_type public_key_b;
                        scvault::crypto::secret_key_type secret_key_b;

                        REQUIRE(generate_key_pair(public_key_b, secret_key_b));
                        REQUIRE(shared_file.share(public_key_b));

                        AND_THEN("The new user can also open the file")
                        {
                            scvault::shared_file_type shared_file_b(path, public_key_b, secret_key_b);
                            std::vector<scvault::byte_type> read_data(10);

                            REQUIRE(shared_file_b.read(read_data.data(), read_data.size()));
                            REQUIRE(data == read_data);

                            AND_THEN("The file can only be shared once")
                            {
                                scvault::result_type<> result = shared_file.share(public_key_b);

                                REQUIRE_FALSE(result);
                                REQUIRE(result.error() == fmt::format("File at {} is already shared with this user", path.string()));
                            }
                            AND_THEN("Only the owner can share the file")
                            {
                                scvault::result_type<> result = shared_file_b.share(scvault::crypto::public_key_type());

                                REQUIRE_FALSE(result);
                                REQUIRE(result.error() == fmt::format("Only the owner can share the file at {}", path.string()));
                            }
                            AND_THEN("Only the owner can unshare the file")
                            {
                                scvault::result_type<> result = shared_file_b.unshare(public_key);

                                REQUIRE_FALSE(result);
                                REQUIRE(result.error() == fmt::format("Only the owner can unshare the file at {}", path.string()));
                                REQUIRE(shared_file.unshare(public_key_b));

                                AND_THEN("The new user cannot open the file anymore")
                                {
                                    // TODO: Reopening of the file would not be needed, if the footers were not cached in the shared_file
                                    REQUIRE_THROWS_AS(scvault::shared_file_type(path, public_key_b, secret_key_b), scvault::exception_type);
                                }
                            }
                            AND_THEN("Unshare only works with users having the file")
                            {
                                scvault::result_type<> result = shared_file.unshare(scvault::crypto::public_key_type());

                                REQUIRE_FALSE(result);
                                REQUIRE(result.error() == fmt::format("File at {} is not shared with this user", path.string()));
                            }
                            AND_THEN("The owners cannot unshare themselves")
                            {
                                scvault::result_type<> result = shared_file.unshare(public_key);

                                REQUIRE_FALSE(result);
                                REQUIRE(result.error() == "The owners cannot unshare themselves");
                            }
                        }
                    }
                }
            }
        }
    }
}
